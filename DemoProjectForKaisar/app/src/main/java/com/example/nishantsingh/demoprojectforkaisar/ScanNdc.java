package com.example.nishantsingh.demoprojectforkaisar;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class ScanNdc extends AppCompatActivity implements View.OnClickListener {

    private Button btnSubmit;
    private Button btnScanner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_ndc);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnSubmit = (Button) findViewById(R.id.btnSubmit);
        btnScanner = (Button) findViewById(R.id.btnScanner);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(getString(R.string.scan_ndc));
        }

        btnSubmit.setOnClickListener(this);
        btnScanner.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnScanner:
                showDialog();

                break;
            case R.id.btnSubmit:
                break;
        }

    }

    private void showDialog() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(ScanNdc.this);
        dialog.setCancelable(false);
        dialog.setTitle("Scan NDC");
        dialog.setMessage("Are you sure you want to Scan NDC?");
        dialog.setPositiveButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                //Action for "Delete".
            }
        })
                .setNegativeButton("YES ", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //Action for "Cancel".
                    }
                });

        final AlertDialog alert = dialog.create();
        alert.show();
    }

}
